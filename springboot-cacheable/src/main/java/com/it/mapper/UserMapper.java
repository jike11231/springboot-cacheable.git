package com.it.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import com.it.annotation.RequiredPermission;
import com.it.entity.User;

import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 用户mapper
 *
 * @author hua
 */
@Repository
public interface UserMapper extends BaseMapper<User> {

}