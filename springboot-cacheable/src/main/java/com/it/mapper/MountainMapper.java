package com.it.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import com.it.annotation.RequiredPermission;
import com.it.entity.Mountain;

import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface MountainMapper extends BaseMapper<Mountain> {
    @RequiredPermission("check")
    @Select("select * from mountain")
    List<Mountain> queryAllUser();
}
