package com.it.entity;

import lombok.Data;

/**
 * 上古神山
 */
@Data
public class Mountain {
    private Integer id;
    private String name;
    private String profile;
    private String address;
}
