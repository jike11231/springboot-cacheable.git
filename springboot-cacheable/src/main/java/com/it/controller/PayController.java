package com.it.controller;

import com.it.service.PayParserFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @description: 支付模块控制层
 */
@RestController
@RequestMapping("/pay")
public class PayController {

    @Autowired
    private PayParserFactory payParserFactory;

    @PostMapping("/orderPay")
    public boolean orderPay(@RequestParam String payType) {
        // 关键点，直接根据类型获取
        boolean result = payParserFactory.getPayParser(payType).pay("P202303013475492");
        return result;
    }
}