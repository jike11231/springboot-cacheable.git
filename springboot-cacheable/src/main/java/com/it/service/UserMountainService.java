package com.it.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.it.entity.Mountain;

import java.util.List;

public interface UserMountainService extends IService<Mountain> {

    /**
     * 获取所有上古神山相关人员信息
     * @return List
     */
    List<Mountain> queryAllUser();
}
