package com.it.service.impl;

import com.it.service.PayService;
import org.springframework.stereotype.Component;

/**
 * @description: 支付宝支付实现层
 */
@Component("AliPay")
public class AliPayServiceImpl implements PayService {
    @Override
    public boolean pay(String orderId) {
        System.out.println("支付宝支付-----------------------");
        return true;
    }
}
