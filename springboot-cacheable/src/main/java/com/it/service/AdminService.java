package com.it.service;

import com.it.entity.User;

import java.util.List;

/**
 * AdminService
 *
 * @author hua
 **/
public interface AdminService {

    void insert1();

    void insert2(User user);

    User insert3();

    List<String> getList(String skip);
}
