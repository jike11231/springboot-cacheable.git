package com.it.service;

import org.springframework.stereotype.Component;

/**
 * @description: 支付服务定位器工厂接口
 */
@Component
public interface PayParserFactory {
    /**
     * 服务定位器
     * @param payType 支付类型
     * @return 返回具体的支付处理实现类
     */
    PayService getPayParser(String payType);
}