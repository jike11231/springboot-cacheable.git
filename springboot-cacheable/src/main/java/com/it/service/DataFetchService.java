package com.it.service;

import com.it.entity.User;

import java.util.List;

/**
 * 获取数据
 */
public interface DataFetchService {
    /**
     * 获取数据
     * @return List<User>
     */
    List<User> dataFetchTask();
}
