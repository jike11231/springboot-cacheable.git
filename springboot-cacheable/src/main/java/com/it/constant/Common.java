package com.it.constant;

/**
 * 公共静态字段
 */
public class Common {
    /*
    * 校验权限
    * */
    public static final String CHECK = "check";
    /*
     * 不校验权限
     * */
    public static final String UN_CHECK = "uncheck";
}
