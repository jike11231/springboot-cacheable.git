package com.it;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Solution {
    public int[] twoSum(int[]nums, int target){

        int []ret=new int[2];//用来存放下标的数组
        int len=nums.length;
        Map<Integer,Integer> hashmap=new HashMap<>();//用来存放整数数组里面的元素及其下标，元素为Key，下标为Value
        hashmap.put(nums[0],0);//先把num[0]存进哈希表
        for(int i=1;i<len;i++){
            //从下标为1的数组元素开始进行存储再查找的操作
            //一.查找
            //1.算出target-num[i]的值
            int differ=target-nums[i];
            //2.查找哈希表中是否存在这个值,我们把数组里的元素当作了key可用contanisKey的方法查找
            if(hashmap.containsKey(differ)){
                ret[0]=hashmap.get(differ);//用key值获取下标
                ret[1]=i;
            }
            //二.存储
            hashmap.put(nums[i],i);

        }
        return ret;
    }
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        //输入数组
        String str=sc.nextLine().toString();//输入数组的内容，可以读取一整行，包括空格
        str=str.replaceFirst("\\[","");
        str=str.replaceFirst("\\]","");
        String arr[]=str.split(",");//拆分字符串成字符串数组
        int []aw=new int [arr.length];//定义一个长度和arr数组相同的int型的数组
        for(int i=0;i<aw.length;i++){
            aw[i]=Integer.parseInt(arr[i]);//将arr的内容复制到aw中
        }
        //输入target
        int target=sc.nextInt();
        //创建对象调用方法
        Solution s1=new Solution();
        System.out.println(s1.twoSum(aw,target));
    }

}
